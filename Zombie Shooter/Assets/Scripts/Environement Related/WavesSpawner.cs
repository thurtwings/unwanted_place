using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames.TPS
{
    public class WavesSpawner : MonoBehaviour
    {
        public int EnemiesAlive = 0;
        public RandomizeShopPosition shopTime;
        public Wave[] waves;
        [SerializeField] private List<Transform> spawnPoints = new List<Transform>();
        [SerializeField] private float timeBetweenWaves = 5f;

        public List<float> distanceFromPlayer = new List<float>();
        private float countdown = 5f;
        public float healthAdder = 1f;
        public bool itsShopTime = false;
        public int waveIndex = 0;
        private void Start()
        {
            shopTime = GetComponent<RandomizeShopPosition>();
            
            EnemiesAlive = 0;
        }
        void Update()
        {

            shopTime.RandomPosition(distanceFromPlayer, spawnPoints);
            
            if (EnemiesAlive > 0)
            {
                return;
            }
            

            if (waveIndex == waves.Length)
            {
                
                this.enabled = false;
            }
            if(waveIndex == waves.Length && EnemiesAlive == 0)
            {
                StartCoroutine(ShopAppears());
            }
            
            if (countdown <= 0f)
            {
                StartCoroutine(SpawnWave());
                countdown = timeBetweenWaves;
                return;
            }

            countdown -= Time.deltaTime;
            countdown = Mathf.Clamp(countdown, 0f, Mathf.Infinity);
           
        }
        IEnumerator ShopAppears()
        {
            shopTime.RandomPosition(shopTime.distanceFromPlayer,shopTime.spawners);
            yield return new WaitForSeconds(.1f);
            shopTime.OrderList(shopTime.distanceFromPlayer, shopTime.spawners);
            yield return new WaitForSeconds(.1f);
            shopTime.ActivateShop();
            yield return new WaitForSeconds(timeBetweenWaves);
            shopTime.DeactivateShop();
        }
        IEnumerator SpawnWave()
        {
            Wave wave = waves[waveIndex];

            EnemiesAlive = wave.count * 3;

            for (int i = 0; i < wave.count; i++)
            {
                SpawnEnemy(wave.enemies[Random.Range(0, wave.enemies.Length - 1)]);
                yield return new WaitForSeconds(1f / wave.rate);
            }

            waveIndex++;
            healthAdder += 50;

        }
        IEnumerator TimerBetweenTwoSpawn()
        {
            yield return new WaitForSeconds(0.1f);
        }
        void SpawnEnemy(GameObject enemy)
        {

            shopTime.OrderList(distanceFromPlayer, spawnPoints);
            int i = 0;
            foreach (Transform spawnPoint in spawnPoints)
            {
                if(i < spawnPoints.Count - 4)
                {
                    
                    StartCoroutine(TimerBetweenTwoSpawn());
                    Instantiate(enemy, spawnPoint.position, spawnPoint.rotation);
                    if (waveIndex > 0)
                    {
                        enemy.GetComponent<HealthZombie>().maxHealth += healthAdder;
                    }

                }
                i++;
                //EnemiesAlive++;
            }
            //for (int i = 0; i < spawnPoints.Count - 4; i++)
            //{

            //    Debug.Log(spawnPoints[i].gameObject.name + " " + distanceFromPlayer[i]);
            //    StartCoroutine(TimerBetweenTwoSpawn());
            //    Instantiate(enemy, spawnPoints[i].position, spawnPoints[i].rotation);
            //    if (waveIndex > 0)
            //    {
            //        enemy.GetComponent<HealthZombie>().maxHealth += healthAdder;
            //    }
            //}


        }



        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, .5f);
        }

    }
}
