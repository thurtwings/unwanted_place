using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;
using UnityEngine.SceneManagement;

using UnityEngine.UI;

namespace ThurtwingsGames.TPS
{
    public class GameManager : MonoBehaviour
    {
        public static GameManager instance;
        public Animator coinController;
        public Animator playerController;
        public TMPro.TMP_Text coinCounter;
        public int creditSaved = 0;
        public bool isDead;
        public Rig bodyRig;
        public Rig aimingRig;
        public Rig handIKRig;
        public GameObject gameOverPanel;



        private void Awake()
        {
            instance = this;
        }
        private void Start()
        {
            isDead = false;
            coinController = coinController.GetComponent<Animator>();
            playerController = playerController.GetComponent<Animator>();
            gameOverPanel.SetActive(false);
        }

        private void Update()
        {
            coinCounter.text = creditSaved.ToString();
            if (isDead)
            {
                GameOver();
            }
        }
        private void GameOver()
        {
            playerController.Play("Death");
            bodyRig.weight = 0;
            aimingRig.weight = 0;
            handIKRig.weight = 0;
            gameOverPanel.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
        }
        public void Pause()
        {

        }
        public void Retry()
        {
            SceneManager.LoadScene(0);
        }

        public void Quit()
        {
            if (UnityEditor.EditorApplication.isPlaying == true)
            {
                UnityEditor.EditorApplication.isPlaying = false;
            }
            else
            {
                Application.Quit();
            }
        }
    }
}
