using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames.TPS
{
    public class RandomizeShopPosition : MonoBehaviour
    {
        public Transform player;
        public List<float> distanceFromPlayer;
        public List<Transform> spawners;
        bool finishOrdering;
        public Dictionary<float, Transform> bestShopPosition = new Dictionary<float, Transform>();
        // Start is called before the first frame update
        void Start()
        {
            player = player.transform;
            
        }

        
        public void RandomPosition(List<float> distanceFromPlayer, List<Transform> spawners)
        {
            distanceFromPlayer.Clear();
            spawners.Clear();
            //bestShopPosition.Clear();

            foreach (Transform spawner in transform)
            {
                float distance = (spawner.position - player.position).magnitude;
                distanceFromPlayer.Add(distance);
                //bestShopPosition.Add(distance, spawner);
                spawners.Add(spawner);
            }
            finishOrdering = false;


        }


        public void OrderList(List<float> distanceFromPlayer, List<Transform> spawners)
        {
            while (!finishOrdering)
            {
                finishOrdering = true;

                for (int i = 0; i < distanceFromPlayer.Count -1; i++)
                {
                    if(distanceFromPlayer[i]> distanceFromPlayer[i + 1])
                    {
                        finishOrdering = false;
                        float temp = distanceFromPlayer[i + 1];
                        Transform tempSpawner = spawners[i + 1];
                        distanceFromPlayer[i + 1] = distanceFromPlayer[i];
                        spawners[i + 1] = spawners[i];
                        distanceFromPlayer[i] = temp;
                        spawners[i] = tempSpawner;
                    }
                }

                //for (int i = 0; i < bestShopPosition.Keys.Count-1; i++)
                //{
                //    for (int j = 0; j < bestShopPosition.Values.Count-1; j++)
                //    {
                //        //finishOrdering = false;
                //        //float temp = bestShopPosition.Keys[i + 1];
                //        //Transform tempSpawner = spawners[i + 1];
                //        //distanceFromPlayer[i + 1] = distanceFromPlayer[i];
                //        //spawners[i + 1] = spawners[i];
                //        //distanceFromPlayer[i] = temp;
                //        //spawners[i] = tempSpawner;
                //    }
                //}
            }
        }

        public void ActivateShop()
        {
            spawners[spawners.Count-1].GetChild(0).gameObject.SetActive(true);
            Debug.Log("Activated");
            

        }public void DeactivateShop()
        {
            spawners[spawners.Count-1].GetChild(0).gameObject.SetActive(false);
            Debug.Log("Deactivated");

        }
    }
}
