using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ThurtwingsGames.TPS
{
    public class PlayerUI : MonoBehaviour
    {
        public Image foregroundImage;
        public Image backgroundImage;
        // Start is called before the first frame update
        public void SetHealthbarPercentage(float percentage)
        {
            float parentWidth = GetComponent<RectTransform>().rect.width;
            float width = parentWidth * percentage;
            foregroundImage.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
        }
    }
}
