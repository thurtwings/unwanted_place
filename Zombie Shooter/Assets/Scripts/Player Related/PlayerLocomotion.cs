using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames.TPS
{
    public class PlayerLocomotion : MonoBehaviour
    {
        public Animator rigController;
        Animator animator;
        CharacterController cc;
        ActiveWeapon activeWeapon;
        ReloadWeapon reloadWeapon;
        Vector2 inputs;

        public float jumpForce;
        public float gravity;
        public float stepDown;
        public float airControl;
        public float jumpDamp;
        public float groundSpeed;
        public float pushPower = 2.0f;
        Vector3 rootMotion;
        Vector3 velocity;
        bool isJumping;
        int isSprinting = Animator.StringToHash("isSprinting");
        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            cc = GetComponent<CharacterController>();
            activeWeapon = GetComponent<ActiveWeapon>();
            reloadWeapon = GetComponent<ReloadWeapon>();
        }

        // Update is called once per frame
        void Update()
        {
            if (!GameManager.instance.isDead)
            {
                inputs.x = Input.GetAxis("Horizontal");
                inputs.y = Input.GetAxis("Vertical");

                animator.SetFloat("Horizontal", inputs.x);
                animator.SetFloat("Vertical", inputs.y);


                UpdateIsSprinting();

                if (Input.GetKeyDown(KeyCode.Space))
                {

                    Jump();
                }
            }
            
        }

        bool IsSprinting()
        {
            bool isSpinting = Input.GetKey(KeyCode.LeftShift);
            bool isFiring = activeWeapon.IsFiring();
            bool isReloading = reloadWeapon.isReloading;
            bool isChangingWeapon = activeWeapon.isChangingWeapon;
            return isSpinting && !isFiring && !isReloading && !isChangingWeapon;
        }
        private void UpdateIsSprinting()
        {
            bool isSpinting = IsSprinting();
            animator.SetBool(isSprinting, isSpinting);
            rigController.SetBool(isSprinting, isSpinting);
        }

        private void OnAnimatorMove()
        {
            rootMotion += animator.deltaPosition;
        }

        private void FixedUpdate()
        {
            if (isJumping)
            {
                UpdateInAir();
            }
            else
            {
                UpdateOnGround();
            }

        }

        private void UpdateOnGround()
        {
            Vector3 stepForwardAmount = rootMotion * groundSpeed;
            Vector3 stepDownAmount = Vector3.down * stepDown;

            cc.Move(stepForwardAmount + stepDownAmount);
            rootMotion = Vector3.zero;

            if (!cc.isGrounded)
            {
                SetInAir(0);
            }
        }

        private void UpdateInAir()
        {
            velocity.y -= gravity * Time.fixedDeltaTime;
            Vector3 displacement = velocity * Time.fixedDeltaTime;
            displacement += CalculateAirControl();

            cc.Move(displacement);

            isJumping = !cc.isGrounded;
            rootMotion = Vector3.zero;
            animator.SetBool("isJumping", isJumping);
        }
        Vector3 CalculateAirControl()
        {
            return ((transform.forward * inputs.y) + (transform.right * inputs.x)) * (airControl / 100);
        }
        void Jump()
        {
            if (!isJumping)
            {
                float jumpVelocity = Mathf.Sqrt(2 * gravity * jumpForce);
                SetInAir(jumpVelocity);
            }
        }

        private void SetInAir(float jumpVelocity)
        {
            isJumping = true;
            velocity = animator.velocity * jumpDamp * groundSpeed;
            velocity.y = jumpVelocity;
            animator.SetBool("isJumping", true);
        }


        //thanks doc unity
        void OnControllerColliderHit(ControllerColliderHit hit)
        {
            Rigidbody body = hit.collider.attachedRigidbody;

            // no rigidbody
            if (body == null || body.isKinematic || GameManager.instance.isDead)
                return;

            // We dont want to push objects below us
            if (hit.moveDirection.y < -0.3f)
                return;

            // Calculate push direction from move direction,
            // we only push objects to the sides never up and down
            Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);

            // If you know how fast your character is trying to move,
            // then you can also multiply the push velocity by that.

            // Apply the push
            body.velocity = pushDir * pushPower;
        }


    }
}
