using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames.TPS
{
    public class PlayerHealth : MonoBehaviour
    {
        public float maxHealth;
        public PlayerUI playerUI;
        public float currentHealth;
        float timerRegen;
        public float maxTimer = 5;
        public float amountPerSec = 5;
        // Start is called before the first frame update
        void Start()
        {
            playerUI = playerUI.GetComponent<PlayerUI>();
            currentHealth = maxHealth;


        }

        private void Update()
        {
            if (!GameManager.instance.isDead)
            {
                timerRegen -= Time.deltaTime;
                if (timerRegen <= 0.0f)
                {

                    currentHealth += amountPerSec * Time.deltaTime;
                    playerUI.SetHealthbarPercentage(currentHealth / maxHealth);

                }
                currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
            }
           
        }

        public void TakeDamages(float damages)
        {
            timerRegen = maxTimer;
            currentHealth -= damages;
            playerUI.SetHealthbarPercentage(currentHealth / maxHealth);
            if (currentHealth <= 0.0f)
            {
                Die();
            }
            
        }

        
        private void Die()
        {
            GameManager.instance.isDead = true;
        }
    }
}
