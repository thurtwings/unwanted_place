using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

namespace ThurtwingsGames.TPS
{
    public class PlayerAiming : MonoBehaviour
    {
        public float turnSpeed = 15f;
        Camera cam;
        public Rig aimLayer;
        public float aimDuration = 0.3f;
        
        // Start is called before the first frame update
        void Start()
        {
            cam = Camera.main;
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
           
        }

        // Update is called once per frame
        void Update()
        {
            if (!GameManager.instance.isDead)
            {
                float yawCamera = cam.transform.rotation.eulerAngles.y;

                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, yawCamera, 0), turnSpeed * Time.fixedDeltaTime);
            }
            
            
        }
    }
}
