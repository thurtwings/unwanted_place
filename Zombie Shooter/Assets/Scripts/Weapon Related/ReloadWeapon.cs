using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames.TPS
{
    public class ReloadWeapon : MonoBehaviour
    {
        public Animator rigController;
        public WeaponAnimationsEvents animationsEvents;
        public ActiveWeapon activeWeapon;
        public AmmoWidget ammoWidget;
        public Transform leftHand;
        public bool isReloading;
        GameObject magazineHand;
        // Start is called before the first frame update
        void Start()
        {
            animationsEvents.weaponAnimationEvent.AddListener(OnAnimationEvents);
        }

        // Update is called once per frame
        void Update()
        {
            RaycastWeapon weapon = activeWeapon.GetActiveWeapon();
            if (weapon)
            {
                if (Input.GetKeyDown(KeyCode.R) || weapon.ammoCount <= 0)
                {
                    isReloading = true;
                    rigController.SetTrigger("Reload_Weapon");
                }

                if (weapon.isFiring)
                {
                    ammoWidget.Refresh(weapon.ammoCount);
                }
            }
            
        }

        void OnAnimationEvents(string eventName)
        {

            switch (eventName)
            {
                case "detach_magazine":
                    DetachMagazine();
                    break;
                case "drop_magazine":
                    DropMagazine();
                    break;
                case "refill_magazine":
                    RefillMagazine();
                    break;
                case "attach_magazine":
                    AttachMagazine();
                    break;

            }
        }

        private void DetachMagazine()
        {
            RaycastWeapon weapon = activeWeapon.GetActiveWeapon();
            magazineHand = Instantiate(weapon.magazine, leftHand, true);
            weapon.magazine.SetActive(false);
        }
        private void DropMagazine()
        {
            GameObject droppedMagazine = Instantiate(magazineHand, magazineHand.transform.position, magazineHand.transform.rotation);
            droppedMagazine.AddComponent<Rigidbody>();
            droppedMagazine.AddComponent<BoxCollider>();
            
            magazineHand.SetActive(false);
        }
        private void RefillMagazine()
        {
            magazineHand.SetActive(true);
        }
        private void AttachMagazine()
        {
            RaycastWeapon weapon = activeWeapon.GetActiveWeapon();
            weapon.magazine.SetActive(true);
            Destroy(magazineHand);
            weapon.ammoCount = weapon.clipSize;
            rigController.ResetTrigger("Reload_Weapon");
            isReloading = false;
            ammoWidget.Refresh(weapon.ammoCount);
        }














    }
}
