using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ThurtwingsGames.TPS
{
    public class AnimationEvents: UnityEvent<string>
    {

    }


    public class WeaponAnimationsEvents : MonoBehaviour
    {
        public AnimationEvents weaponAnimationEvent = new AnimationEvents();
       public void OnAnimationEvents(string eventName)
        {
            weaponAnimationEvent.Invoke(eventName);
        }
    }
}
