using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Animations.Rigging;
using Cinemachine;

namespace ThurtwingsGames.TPS
{
    public class ActiveWeapon : MonoBehaviour
    {
        public enum WeaponSlot
        {
            Primary = 0,
            Secondary = 1,
            Sword = 2,
        }
        public Transform crosshairTarget;
        RaycastWeapon[] equipped_Weapons = new RaycastWeapon[2];
        int activeWeaponIndex;
        public Rig handIk;
        public Transform[] weaponSlots;
        public CinemachineFreeLook playerCamera;
        public Image activeWeaponIcon;
        public Image activeWeaponAmmoIcon;
        public Transform weaponLeftGrip;
        public Transform weaponRightGrip;
        public AmmoWidget ammoWidget;
        public Animator rigController;
        bool isHolstered = false;
        public bool isChangingWeapon;
        // Start is called before the first frame update
        void Start()
        {
            
           RaycastWeapon existingWeapon = GetComponentInChildren<RaycastWeapon>();
            if (existingWeapon)
            {
                Equip(existingWeapon);
            }
        }
        public RaycastWeapon GetActiveWeapon()
        {
            return GetWeapon(activeWeaponIndex);
        }
        RaycastWeapon GetWeapon(int index)
        {
            if (index < 0 || index >= equipped_Weapons.Length)
            {
                return null;
            }

            return equipped_Weapons[index];
        }
       
        void Update()
        {
            var weapon = GetWeapon(activeWeaponIndex);
            bool notSprinting = rigController.GetCurrentAnimatorStateInfo(2).shortNameHash == Animator.StringToHash("NotSprinting");
            if (weapon && !isHolstered && notSprinting)
            {
                
                weapon.UpdateWeapon(Time.deltaTime);
                if (weapon.weaponName == "LightRiffle")
                {
                    rigController.SetInteger("Weapon_Index", 0);
                    
                }
                else if (weapon.weaponName == "Pistol")
                {
                    rigController.SetInteger("Weapon_Index", 1);
                    
                }
                else if (weapon.weaponName == "HeavyRiffle")
                {
                    rigController.SetInteger("Weapon_Index", 2);
                    
                }
            }
            

            if (Input.GetKeyDown(KeyCode.X)) { ToogleActiveWeapon(); }

            if (Input.GetKeyDown(KeyCode.Alpha1)) { SetActiveWeapon(WeaponSlot.Primary); }

            if (Input.GetKeyDown(KeyCode.Alpha2)) { SetActiveWeapon(WeaponSlot.Secondary); }

            //if (Input.GetKeyDown(KeyCode.Alpha3)) { SetActiveWeapon(WeaponSlot.Sword); }
            if (GameManager.instance.isDead)
            {
                if (weapon)
                {
                    weapon.transform.parent = null;
                    rigController.gameObject.SetActive(false);
                }
                
            }

        }

        public bool IsFiring()
        {
            RaycastWeapon currentWeapon = GetActiveWeapon();
            if (!currentWeapon)
            {
                return false;
            }

            return currentWeapon.isFiring;
        }
        public void Equip(RaycastWeapon newWeapon)
        {
            int weaponSlotIndex = (int)newWeapon.weaponSlot;
            var weapon = GetWeapon(weaponSlotIndex);

            if (weapon)
            {
                Destroy(weapon.gameObject);
            }
            weapon = newWeapon;
            weapon.rayCastDestination = crosshairTarget;
            weapon.recoil.playerCamera = playerCamera;
            weapon.recoil.rigController = rigController;
            weapon.transform.SetParent(weaponSlots[weaponSlotIndex],false);
            
            equipped_Weapons[weaponSlotIndex] = weapon;
            
            SetActiveWeapon(newWeapon.weaponSlot);
            activeWeaponIcon.sprite = weapon.iconWeapon;
            activeWeaponAmmoIcon.sprite = weapon.iconAmmo;
            ammoWidget.Refresh(weapon.ammoCount);
        }

        void SetActiveWeapon(WeaponSlot weaponSlot)
        {
            int holsterIndex = activeWeaponIndex;
            int activateIndex = (int)weaponSlot;
            
            if (holsterIndex == activateIndex)
            {
                holsterIndex = -1;
            }
            StartCoroutine(SwitchWeapon(holsterIndex, activateIndex));
            
        }

        void ToogleActiveWeapon()
        {
            bool isHolstered = rigController.GetBool("holster_Weapon");

            if (isHolstered)
            {
                StartCoroutine(ActivateWeapon(activeWeaponIndex));
            }
            else
            {
                StartCoroutine(HolsterWeapon(activeWeaponIndex));
            }
            
        }
        IEnumerator SwitchWeapon(int holsterIndex, int activateIndex)
        {
            //rigController.SetInteger("Weapon_Index", activateIndex);

            yield return StartCoroutine(HolsterWeapon(holsterIndex));
            yield return StartCoroutine(ActivateWeapon(activateIndex));
            activeWeaponIndex = activateIndex;
        }
        IEnumerator HolsterWeapon(int index)
        {
            isChangingWeapon = true;
            isHolstered = true;
            var weapon = GetWeapon(index);
            if (weapon)
            {
                rigController.SetBool("holster_Weapon", true);
                do
                {
                    yield return new WaitForEndOfFrame();

                } 
                while (rigController.GetCurrentAnimatorStateInfo(0).normalizedTime < 1);
                
            }
            isChangingWeapon = false;
            //rigController.SetInteger("Weapon_Index", 0);
        }
        IEnumerator ActivateWeapon(int index)
        {
            isChangingWeapon = true;
            var weapon = GetWeapon(index);
            if (weapon)
            {
                rigController.SetBool("holster_Weapon", false);
                rigController.Play("Equip_" + weapon.weaponName);
                do
                {
                    yield return new WaitForEndOfFrame();

                }
                while (rigController.GetCurrentAnimatorStateInfo(0).normalizedTime < 1);
                isHolstered = false;
                activeWeaponIcon.sprite = weapon.iconWeapon;
                activeWeaponAmmoIcon.sprite = weapon.iconAmmo;
                ammoWidget.Refresh(weapon.ammoCount);
            }
            isChangingWeapon = false;
        }
    }
}
