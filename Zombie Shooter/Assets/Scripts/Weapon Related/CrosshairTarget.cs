using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames.TPS
{
    public class CrosshairTarget : MonoBehaviour
    {

        Camera mainCam;
        Ray ray;
        RaycastHit hit;

        // Start is called before the first frame update
        void Start()
        {
            mainCam = Camera.main;
        }

        // Update is called once per frame
        void Update()
        {
            ray.origin = mainCam.transform.position;
            ray.direction = mainCam.transform.forward;
            
            if (Physics.Raycast(ray, out hit))
            {
                transform.position = hit.point;
            }
            else
            {
                transform.position = ray.origin + ray.direction * 1000.0f;
            }
            
        }
    }
}
