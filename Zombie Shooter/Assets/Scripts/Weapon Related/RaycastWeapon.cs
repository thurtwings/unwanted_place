using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace ThurtwingsGames.TPS
{
    public class RaycastWeapon : MonoBehaviour
    {
        class Bullet
        {
            public float time;

            public Vector3 initialPosition;
            public Vector3 initialVelocity;

            public TrailRenderer tracer;
            public int bounce;

        }

        
        public ActiveWeapon.WeaponSlot weaponSlot;
        public bool isFiring = false;

        public int fireRate = 25;

        public float bulletSpeed = 1000.0f;
        public float bulletDrop = 0.0f;
        public int maxBounces = 0;
        public float impulseForce = 20f;
        float accumulatedTime;

        public ParticleSystem muzzleFlash;
        public ParticleSystem hitEffect;

        public TrailRenderer tracerEffect;

        public string weaponName;
        public Sprite iconWeapon;
        public Sprite iconAmmo;
        public GameObject magazine;
        public float damages = 10f;
        public Transform rayCastOrigin;
        public Transform rayCastDestination;
        public WeaponRecoil recoil;
        Ray ray;
        RaycastHit hit;
        public int ammoCount;
        public int clipSize;

        List<Bullet> bullets = new List<Bullet>();
        float maxLifeTime = 3.0f;
        private void Awake()
        {
            recoil = GetComponent<WeaponRecoil>();
        }
        Vector3 GetPosition(Bullet bullet)
        {
            // pos + vel*time +0.5*gravity * time*time
            Vector3 gravity = Vector3.down * bulletDrop;
            return (bullet.initialPosition) + (bullet.initialVelocity * bullet.time) + (0.5f * gravity * bullet.time * bullet.time);
        }

        Bullet CreateBullet(Vector3 position, Vector3 velocity)
        {
            Bullet bullet = new Bullet();
            bullet.initialPosition = position;
            bullet.initialVelocity = velocity;
            bullet.time = 0.0f;
            bullet.tracer = Instantiate(tracerEffect, position, Quaternion.identity);
            bullet.tracer.AddPosition(position);
            bullet.bounce = maxBounces;
            Color color = Random.ColorHSV(0.46f, 0.61f);
            float intensity = 20f;
            Color rgb = new Color(color.r * intensity, color.g * intensity, color.b * intensity, color.a * intensity);
            bullet.tracer.material.SetColor("_EmissionColor", rgb);
            return bullet;
        }
        public void StartFiring()
        {
            isFiring = true;
            accumulatedTime = 0.0f;
            
        }
        public void UpdateWeapon(float deltatime)
        {
            if (!GameManager.instance.isDead)
            {
                if (Input.GetMouseButtonDown(0))
                {

                    StartFiring();
                }

                if (isFiring) { UpdateFiring(deltatime); }

                UpdateBullet(deltatime);

                if (Input.GetMouseButtonUp(0))
                {
                    StopFiring();
                }
            }
        }
        public void UpdateFiring(float deltaTime)
        {
            accumulatedTime += deltaTime;

            float fireInterval = 1.0f / fireRate;

            while(accumulatedTime >= 0.0f)
            {
                FireBullet();
                accumulatedTime -= fireInterval;
            }
        }
        public void UpdateBullet(float deltaTime)
        {
            SimulateBullets(deltaTime);
            DestroysBullet();
        }
        private void SimulateBullets(float deltaTime)
        {
            bullets.ForEach(bullet => {
                Vector3 p0 = GetPosition(bullet);
                bullet.time += deltaTime;
                Vector3 p1 = GetPosition(bullet);
                RaycastSegment(p0, p1, bullet);
            });
        }
        void DestroysBullet()
        {
            bullets.RemoveAll(bullet => bullet.time >= maxLifeTime);
        }
        void RaycastSegment(Vector3 start, Vector3 end, Bullet bullet)
        {
            Vector3 direction = end - start;
            float distance = direction.magnitude;
            ray.origin = start;
            ray.direction = direction;


            if (Physics.Raycast(ray, out hit, distance))
            {
                hitEffect.transform.position = hit.point;
                hitEffect.transform.forward = hit.normal;
                hitEffect.Emit(1);
                //hitEffect.transform.parent = hit.transform;

                //bullet.tracer.transform.position = hit.point;
                bullet.time = maxLifeTime;
                end = hit.point;

                //ricochets
                if(bullet.bounce > 0)
                {
                    bullet.time = 0;
                    bullet.initialPosition = hit.point;
                    bullet.initialVelocity = Vector3.Reflect(bullet.initialVelocity, hit.normal);
                    bullet.bounce--;
                }

                //Collision impulse
                var rb = hit.collider.GetComponent<Rigidbody>();
                if (rb)
                {
                    rb.AddForceAtPosition(ray.direction * impulseForce, hit.point, ForceMode.Impulse);
                }

                var hitBox = hit.collider.GetComponent<HitBox>();
                if (hitBox)
                {
                    hitBox.OnRaycastHit(this, ray.direction);
                }
            }
            bullet.tracer.transform.position = end;
            
        }
        public void FireBullet()
        {

            if(ammoCount <= 0)
            {
                return;
            }
            ammoCount--; 
            muzzleFlash.Emit(1);


            Vector3 velocity = (rayCastDestination.position - rayCastOrigin.position).normalized * bulletSpeed;
            var bullet = CreateBullet(rayCastOrigin.position, velocity);

            bullets.Add(bullet);

            recoil.GenerateRecoil(weaponName);
        }
        public void StopFiring()
        {
            isFiring = false;
            recoil.Reset();
        }
    }
}
