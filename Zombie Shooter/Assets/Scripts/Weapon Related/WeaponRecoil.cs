using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

namespace ThurtwingsGames.TPS
{
    public class WeaponRecoil : MonoBehaviour
    {
        [HideInInspector]
        public CinemachineFreeLook playerCamera;
        [HideInInspector]
        public CinemachineImpulseSource cameraShake;
        [HideInInspector]
        public Animator rigController;
        float verticalRecoil;
        public float minverticalRecoil;
        public float maxVerticalRecoil;
        
        float horizontalRecoil;
        public float minHorizontalRecoil;
        public float maxHorizontalRecoil;
        //public Transform camInitPos;
        public float duration;
        float time;
        private void Awake()
        {
            cameraShake = GetComponent<CinemachineImpulseSource>();
        }

        public void Reset()
        {
            verticalRecoil = 0.0f;
            horizontalRecoil = 0.0f;
        }
       
        public void GenerateRecoil(string weaponName)
        {
            
            time = duration;
            verticalRecoil = Random.Range(minverticalRecoil, maxVerticalRecoil);
            horizontalRecoil = Random.Range(minHorizontalRecoil, maxHorizontalRecoil);
            cameraShake.GenerateImpulse(Camera.main.transform.forward);
            rigController.Play("Weapon_Recoil_" + weaponName, 1, 0.0f);
        }

        // Update is called once per frame
        void Update()
        {
            if(time > 0)
            {
                playerCamera.m_YAxis.Value -= (verticalRecoil * Time.deltaTime) / duration;
                playerCamera.m_XAxis.Value -= (horizontalRecoil * Time.deltaTime) / duration;
                time -= Time.deltaTime;
            }
        }
    }
}
