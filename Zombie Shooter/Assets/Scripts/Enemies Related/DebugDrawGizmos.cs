using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace ThurtwingsGames.TPS
{
    public class DebugDrawGizmos : MonoBehaviour
    {
        public bool velocity;
        public bool desiredVelocity;
        public bool path;

        public NavMeshAgent navMeshAgent;
        
        // Start is called before the first frame update
        void Start()
        {
            navMeshAgent = navMeshAgent.GetComponent<NavMeshAgent>();
            velocity = true;
            desiredVelocity = true;
            path = true;
        }
     
        // Update is called once per frame
        void OnDrawGizmos()
        {
            if (velocity)
            {
                Gizmos.color = Color.green;
                Debug.DrawLine(transform.position, transform.position + navMeshAgent.velocity);
            }

            if (desiredVelocity)
            {
                Gizmos.color = Color.red;
                Debug.DrawLine(transform.position, transform.position + navMeshAgent.desiredVelocity);
            }

            if (path)
            {
                Gizmos.color = Color.black;
                var agentPath = navMeshAgent.path;
                Vector3 prevCorner = transform.position;
                foreach (var corner in agentPath.corners)
                {
                    Gizmos.DrawLine(prevCorner, corner);
                    Gizmos.DrawSphere(corner, 0.1f);
                    prevCorner = corner;
                }
            }
        }
    }
}
