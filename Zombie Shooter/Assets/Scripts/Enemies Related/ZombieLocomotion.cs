using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace ThurtwingsGames.TPS
{
    public class ZombieLocomotion : MonoBehaviour
    {
        NavMeshAgent agent;
        public PlayerHealth player;
        Animator animator;
        int agentPrority = 35;
        public float timer, maxTime, maxDistance;
        void Start()
        {
            agent = GetComponent<NavMeshAgent>();
            animator = GetComponent<Animator>();
            agentPrority = Random.Range(1, 100);
            agent.avoidancePriority = agentPrority;
            player = FindObjectOfType<PlayerHealth>();
            agent.speed = Random.Range(0.5f, 3.5f);
        }

        void Update()
        {
            if (!agent.enabled)
                return;

            timer -= Time.deltaTime;
            if(timer < 0.0f)
            {
                float sqrDistance = (player.gameObject.transform.position - transform.position).sqrMagnitude;
                if(sqrDistance > maxDistance * maxDistance)
                {
                    agent.SetDestination(player.gameObject.transform.position);
                }
                timer = maxTime;
            }

            animator.SetFloat("Speed", agent.velocity.magnitude);

        }
    }
}
