using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace ThurtwingsGames.TPS
{
    public class ZombieAttack : MonoBehaviour
    {
        Animator animator;
        public float damages;
        NavMeshAgent agent;
        public int randomPlayerDeathCelebration;
        // Start is called before the first frame update
        void Start()
        {
            
            animator = GetComponent<Animator>();
            agent = GetComponent<NavMeshAgent>();
            animator.SetBool("DanceIsOn", false);
            randomPlayerDeathCelebration = Random.Range(1, 100);
        }

        // Update is called once per frame
        void Update()
        {
            if (!agent.enabled && !this.GetComponent<HealthZombie>().isDead)
                return;

            if (agent.enabled && agent.remainingDistance <= agent.stoppingDistance)
            {
                
                animator.SetTrigger("Attack");
            }
            if (GameManager.instance.isDead)
            {
                agent.velocity = Vector3.zero;
                agent.enabled = false;
                if (randomPlayerDeathCelebration >= 95)
                {
                    animator.SetBool("DanceIsOn", true);
                }
                else
                {
                    animator.SetBool("NormalPlayerDeath", true);
                }
                
                
            }
        }
    }
}
