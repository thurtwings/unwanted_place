using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames.TPS
{
    public class HandTrigger : MonoBehaviour
    {
        ZombieAttack zombieAttack;
        // Start is called before the first frame update
        void Start()
        {
            zombieAttack = GetComponentInParent<ZombieAttack>();
            
        }

        // Update is called once per frame
       

        private void OnTriggerEnter(Collider other)
        {
            PlayerHealth playerHealth = other.GetComponent<PlayerHealth>();
            if (playerHealth)
            {
                playerHealth.TakeDamages(zombieAttack.damages);
            }
        }
    }
}
