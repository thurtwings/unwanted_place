
using UnityEngine;
using UnityEngine.AI;

namespace ThurtwingsGames.TPS
{
    public class HealthZombie : MonoBehaviour
    {
        RagDoll ragdoll;
        SkinnedMeshRenderer[] skinnedMeshRenderer;
        UIHealthBar healthBar;
        public float blinkIntensity;
        public float blinkDuration;
        float blinkTimer;
        public float deathImpulse;
        NavMeshAgent agent;
        public float maxHealth;
        public WavesSpawner wavesSpawner;
        public float currentHealth;
        public bool isDead;
        public int creditForKilling;
        // Start is called before the first frame update
        void Start()
        {

            creditForKilling = Random.Range(1, 10);
            agent = GetComponent<NavMeshAgent>();
            ragdoll = GetComponent<RagDoll>();
            skinnedMeshRenderer = GetComponentsInChildren<SkinnedMeshRenderer>();
            healthBar = GetComponentInChildren<UIHealthBar>();
            wavesSpawner = FindObjectOfType<WavesSpawner>();
            
            var rigidbodies = GetComponentsInChildren<Rigidbody>();
            foreach (var rb in rigidbodies)
            {
                HitBox hitBox = rb.gameObject.AddComponent<HitBox>();
                hitBox.healthZombie = this;
            }
            agent.enabled = true;
            isDead = false;
            if(wavesSpawner.waveIndex > 0)
            {
                currentHealth = maxHealth + wavesSpawner.healthAdder;
            }
            else
            {
                currentHealth = maxHealth;
            }
            
        }

        // Update is called once per frame
        void Update()
        {
            blinkTimer -= Time.deltaTime;
            float lerp = Mathf.Clamp01(blinkTimer / blinkDuration);
            float intensity = (lerp * blinkIntensity) + 1.0f;
            
            foreach (var skinned in skinnedMeshRenderer)
            {
                skinned.material.color = Color.white * intensity;
            }
            
        }

        public void TakeDamages(float damages, Vector3 direction)
        {
            currentHealth -= damages;
            healthBar.SetHealthbarPercentage(currentHealth / maxHealth);
            if (currentHealth <= 0.0f && !isDead)
            {
                isDead = true;
                Die(direction);
                wavesSpawner.EnemiesAlive--;
                GameManager.instance.creditSaved += creditForKilling;
                GameManager.instance.coinController.SetTrigger("SpinCoin");
                
            }
            blinkTimer = blinkDuration;
        }

        private void Die(Vector3 direction)
        {
            ragdoll.ActivateRagdoll();
            direction.y = 1;
            ragdoll.ApplyForce(direction * deathImpulse);
            healthBar.gameObject.SetActive(false);
            agent.enabled = false;
            Destroy(gameObject, 15f);
        }
    }
}
