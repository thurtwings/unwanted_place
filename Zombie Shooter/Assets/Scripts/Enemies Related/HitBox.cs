using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames.TPS
{
    public class HitBox : MonoBehaviour
    {
        public HealthZombie healthZombie;


        public void OnRaycastHit(RaycastWeapon weapon, Vector3 direction)
        {
            healthZombie.TakeDamages(weapon.damages, direction);
        }
        // Start is called before the first frame update
        void Start()
        {
        
        }

        // Update is called once per frame
        void Update()
        {
        
        }
    }
}
