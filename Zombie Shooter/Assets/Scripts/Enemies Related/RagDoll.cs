using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames.TPS
{
    public class RagDoll : MonoBehaviour
    {
        Rigidbody[] rbs;
        Animator animator;
        
        void Start()
        {
            rbs = GetComponentsInChildren<Rigidbody>();
            animator = GetComponent<Animator>();
            DeactivateRagdoll();
        }

        public void DeactivateRagdoll()
        {
            foreach (var rigidbody in rbs)
            {
                rigidbody.isKinematic = true;
            }
            animator.enabled = true;

        }

        public void ActivateRagdoll()
        {
            foreach (var rigidbody in rbs)
            {
                rigidbody.isKinematic = false;
            }
            animator.enabled = false;
        }

        public void ApplyForce(Vector3 force)
        {
            var rigidbody = animator.GetBoneTransform(HumanBodyBones.Hips).GetComponent<Rigidbody>();
            rigidbody.AddForce(force, ForceMode.VelocityChange);
        }
    }
}
