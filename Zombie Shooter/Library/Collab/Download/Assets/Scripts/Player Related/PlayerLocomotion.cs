using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThurtwingsGames.TPS
{
    public class PlayerLocomotion : MonoBehaviour
    {

        Animator animator;
        CharacterController cc;
        Vector2 inputs;

        public float jumpForce;
        public float gravity;
        public float stepDown;
        public float airControl;
        public float jumpDamp;
        public float groundSpeed;
        Vector3 rootMotion;
        Vector3 velocity;
        bool isJumping;
        // Start is called before the first frame update
        void Start()
        {
            animator = GetComponent<Animator>();
            cc = GetComponent<CharacterController>();
        }

        // Update is called once per frame
        void Update()
        {
            inputs.x = Input.GetAxis("Horizontal");
            inputs.y = Input.GetAxis("Vertical");

            animator.SetFloat("Horizontal", inputs.x);
            animator.SetFloat("Vertical", inputs.y);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Jump();
            }
        }

        private void OnAnimatorMove()
        {
            rootMotion += animator.deltaPosition;
        }

        private void FixedUpdate()
        {
            if (isJumping)
            {
                UpdateInAir();
            }
            else
            {
                UpdateOnGround();
            }

        }

        private void UpdateOnGround()
        {
            Vector3 stepForwardAmount = rootMotion * groundSpeed;
            Vector3 stepDownAmount = Vector3.down * stepDown;

            cc.Move(stepForwardAmount + stepDownAmount);
            rootMotion = Vector3.zero;

            if (!cc.isGrounded)
            {
                SetInAir(0);
                
            }
        }

        private void UpdateInAir()
        {
            velocity.y -= gravity * Time.fixedDeltaTime;
            Vector3 displacement = velocity * Time.fixedDeltaTime;
            displacement += CalculateAirControl();

            cc.Move(displacement);

            isJumping = !cc.isGrounded;
            rootMotion = Vector3.zero;
            animator.SetBool("isJumping", isJumping);
        }

        void Jump()
        {
            if (!isJumping)
            {
                float jumpVelocity = Mathf.Sqrt(2 * gravity * jumpForce);
                SetInAir(jumpVelocity);
            }
        }

        private void SetInAir(float jumpVelocity)
        {
            isJumping = true;
            velocity = animator.velocity * jumpDamp * groundSpeed;
            velocity.y = jumpVelocity;
            animator.SetBool("isJumping", true);
        }

        Vector3 CalculateAirControl()
        {
            return ((transform.forward * inputs.y) + (transform.right * inputs.x)) * (airControl/100);
        }

    }
}
